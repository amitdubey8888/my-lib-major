import {Component} from '@angular/core';
import {NavController, NavParams, Platform, LoadingController} from 'ionic-angular';
import {NgForm} from "@angular/forms";
import {Api, User} from '../../providers/providers';
import {Toast} from '@ionic-native/toast';
import {SigninPage} from "../signin/signin";
import {Settings} from "../../providers/settings";
import {dataService} from "../../providers/data.service";

@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {

  userProfile = {
    profile_picture: "",
    full_name: "",
    phone: "",
    email: "",
    password: ""
  };
  password2;
  canEdit:boolean;
  public preferences:any = {background: ''};
  private loader = this.loadingCtrl.create({
    content: "Please wait...",
    dismissOnPageChange: true
  });

  constructor(public navCtrl:NavController,
              public dataService:dataService,
              public navParams:NavParams,
              public user:User,
              public api:Api,
              public toast:Toast,
              public settings:Settings,
              private plt:Platform,
              public loadingCtrl:LoadingController) {
  }

  ionViewWillEnter() {
    if (!this.user.currentUser) {
      this.navCtrl.push(SigninPage)
    } else {
      this.getPreferences();
      this.canEdit = this.user.currentUser.realm == 'user';
      this.userProfile = this.user.currentUser;
    }
  }

  updateProfile(form:NgForm) {
    let details = this.userProfile;
    this.api.put("users/updateUser", {details: details})
      .map(res => res.json())
      .subscribe(
        response => {
          console.info("response", response);
          this.showToast('Profile info changed successfully!');
          this.settings.setValue('profile', this.user.currentUser);
          this.goBack();
        },
        error => console.log(error)
      );
  }

  showToast(message) {
    if (this.plt.is('cordova')) {
      this.toast.show(message, '5000', 'bottom').subscribe(toast => console.log(toast))
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  getPreferences() {
    // this.api.get(`app_preferences?filter[fields][background]=true`)
    //   .map(res => res.json())
    //   .subscribe(res => {
    //     this.preferences = res[0];
    //   });

    this.preferences.background = this.dataService.homeBgPath;
  }
}
