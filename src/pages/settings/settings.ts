import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { ProfilePage } from '../../pages/profile/profile';
import { LicensePage } from '../../pages/license/license';
import { HelpPage } from '../../pages/help/help';
import { PrivacyPage } from '../../pages/privacy/privacy';
import { TermsPage } from '../../pages/terms/terms';
import { NetworkCheckService } from "../../providers/network.check.service";
import { Settings } from "../../providers/settings";
import { Api } from "../../providers/api";
import { SigninPage } from "../signin/signin";
import { User } from "../../providers/user";


import { dataService } from "../../providers/data.service";

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  devHeight: any = {};
  preferences: any = { background: '' };

  constructor(public navCtrl: NavController, public navParams: NavParams, public dataService: dataService,private user: User,
    public api: Api, private network: NetworkCheckService, private settings: Settings,
    public _platform: Platform) { }

  ionViewDidLoad() {
    this.devHeight = this._platform.height();
    console.log('ionViewDidLoad SettingsPage');
  }

  ionViewWillEnter() {
    console.log(this.dataService.homeBgPath);
    this.preferences.background = this.dataService.homeBgPath;
  }
  openHelp() {
    this.navCtrl.push(HelpPage);
  }
  openTerms() {
    this.navCtrl.push(TermsPage);
  }
  openPrivacy() {
    this.navCtrl.push(PrivacyPage);
  }
  openLicense() {
    this.navCtrl.push(LicensePage);
  }
  openProfile() {
    if (!this.user.currentUser) {
      this.dataService.currentStateName = 'ProfilePage';
      this.dataService.currentState = ProfilePage
      this.navCtrl.push(SigninPage);
    } else {
      this.navCtrl.push(ProfilePage);
    }

  }
}
