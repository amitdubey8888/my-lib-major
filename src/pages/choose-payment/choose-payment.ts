import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ChooseNetworkPage } from '../../pages/choose-network/choose-network';
import { CardCheckoutPage } from "../card-checkout/card-checkout";
import { User } from "../../providers/user";
import { Api } from "../../providers/api";
import { dataService } from "../../providers/data.service";

/*
 Generated class for the ChoosePayment page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-choose-payment',
  templateUrl: 'choose-payment.html'
})
export class ChoosePaymentPage implements OnInit {
  paymentMethod = '';
  plan: any;
  public preferences: any = { background: '' };

  constructor(public navCtrl: NavController,
    public dataService: dataService,
    public navParams: NavParams,
    public user: User,
    private api: Api) {
  }

  ngOnInit() {
    this.getPreferences();
    this.plan = this.navParams.get('plan');
    console.log("plan", this.plan)
  }

  goBack() {
    this.navCtrl.pop();
  }

  selectMethod(method) {
    this.paymentMethod = method;
  }

  proceed() {
    if (this.paymentMethod == 'phone') {
      this.navCtrl.push(ChooseNetworkPage, { plan: this.plan });
    }
    else if (this.paymentMethod == 'card') {
      this.navCtrl.push(CardCheckoutPage, { plan: this.plan });
    }
  }

  getPreferences() {
    // this.api.get(`app_preferences?filter[fields][background]=true`)
    //   .map(res => res.json())
    //   .subscribe(res => {
    //     this.preferences = res[0];
    //   });
    this.preferences.background = this.dataService.homeBgPath;
  }
}
