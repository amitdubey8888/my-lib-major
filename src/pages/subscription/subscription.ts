import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Platform, ToastController } from 'ionic-angular';
import { ChoosePaymentPage } from '../../pages/choose-payment/choose-payment';
import { Api } from "../../providers/api";
import * as _ from 'lodash'
import { User } from "../../providers/user";
import { SigninPage } from "../signin/signin";
import { Home2Page } from "../home2/home2";
import { Settings } from "../../providers/settings";
import { dataService } from "../../providers/data.service";

@Component({
  selector: 'page-subscription',
  templateUrl: 'subscription.html'
})
export class SubscriptionPage {
  devHeight: any = {};
  preferences: any = { background: '' };
  public plans: any = [];
  public selectedPlan: any;
  public activePlan: any;
  contentHeight: {};
  private loader = this.loadingCtrl.create({
    content: "Processing...",
    dismissOnPageChange: true
  });

  constructor(public navCtrl: NavController,
    public dataService: dataService,
    public navParams: NavParams,
    private api: Api,
    private user: User,
    private loadingCtrl: LoadingController,
    private toast: ToastController,
    private settings: Settings,
    public _platform: Platform) {

  }

  ionViewWillEnter() {
    this.devHeight = this._platform.height();
    this.contentHeight = { 'max-height': (this.devHeight - 110) + 'px', 'overflow-y': 'scroll' };
    // if (!this.user.currentUser)
    //   this.navCtrl.push(SigninPage);
    // else {
    //   this.getPreferences();
    //   this.fetchPlans();
    // }
    this.getPreferences();
    this.fetchPlans();

  }

  fetchPlans() {
    this.api.get(`subscriptions`)
      .map(res => res.json())
      .subscribe(res => {
        this.plans = res;
        this.fetchActiveSubscription();
      })
  }

  selectPlan(i) {
    this.initializePlans();
    this.plans[i].selected = true;
    this.selectedPlan = this.plans[i]
  }

  choosePayment() {
    if (!this.user.currentUser) {
      this.dataService.currentStateName = 'SubscriptionPage';
      this.dataService.currentState = SubscriptionPage
      this.navCtrl.push(SigninPage);
    } else {
      if (this.selectedPlan) {
        if (this.selectedPlan.subscription_amount)
          this.navCtrl.push(ChoosePaymentPage, { plan: this.selectedPlan });
        else
          this.handleSuccess();
      } else {
        this.showToast('Please choose a plan.');
      }
    }

  }

  getPreferences() {
    this.preferences.background = this.dataService.homeBgPath;
  }

  fetchActiveSubscription() {
    if (this.user.currentUser) {
      if (this.user.currentUser.subscribed_plan && this.user.currentUser.subscribed_plan.id) {
        //noinspection TypeScriptValidateTypes
        this.user.currentUser.subscribed_plan.subscriptionDate = new Date(this.user.currentUser.subscribed_plan.subscriptionDate);
        this.activePlan = this.user.currentUser.subscribed_plan;
        let index = _.findIndex(this.plans, ['id', this.user.currentUser.subscribed_plan.id]);
        if (index >= 0) {
          this.plans[index].selected = true;
          this.selectedPlan = this.plans[index]
        }
        else {
          //_handle plan might have been deleted
        }
      }
    }
  }

  initializePlans() {
    _.forEach(this.plans, plan => {
      plan.selected = false
    })
  }

  handleSuccess() {
    //noinspection TypeScriptUnresolvedFunction
    this.loader.present()
      .then(() => { })
      .catch(() => { });

    this.selectedPlan.subscriptionDate = new Date();
    this.user.currentUser.subscribed_plan = this.selectedPlan;
    this.api.put("users/updateUser", { details: this.user.currentUser })
      .map(res => res.json())
      .subscribe(
      response => {
        this.settings.setValue('profile', this.user.currentUser);
        this.loader.dismiss();
        let length = this.navCtrl.length();
        if (this.navCtrl.getViews().length === 1) {
          this.navCtrl.setRoot(Home2Page);
        }
        else if(this.dataService.currentStateName=='LessonPage') {
          this.navCtrl.push(this.dataService.currentState, this.dataService.currentStateParams)
          this.navCtrl.remove(length - 1);
        }
        else{
          this.navCtrl.pop();
        }
        //this.navCtrl.setRoot(Home2Page);
        this.showToast(`Successfully subscribed to ${this.selectedPlan.subscription_title} plan`)
      },
      error => {
        console.log(error);
        //_handle network check and timeout
        this.handleSuccess();
      }
      );
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }
}