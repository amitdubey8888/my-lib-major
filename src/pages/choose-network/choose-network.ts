import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EnterPhonePage } from '../../pages/enter-phone/enter-phone';

/*
  Generated class for the ChooseNetwork page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-choose-network',
  templateUrl: 'choose-network.html'
})
export class ChooseNetworkPage {
  network = '';

  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChooseNetworkPage');
  }
  goBack() {
    this.navCtrl.pop();
  }
  selectNetwork(network) {
    console.log('network selected ' + network)
    this.network = network;
  }
  enterPhone(){
    this.navCtrl.push(EnterPhonePage);
  }
}
