import {Component} from '@angular/core';
import {NavController, NavParams, LoadingController, ToastController} from 'ionic-angular';
import {Home2Page} from '../../pages/home2/home2';
import {User} from '../../providers/user';
import {Api} from "../../providers/api";
import { dataService } from "../../providers/data.service";
import { SubscriptionPage } from '../../pages/subscription/subscription';
import {BookPage} from '../../pages/book/book';
import {SigninPage} from "../signin/signin";

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  userInfo = {
    full_name: '',
    phone: '',
    email: '',
    password: '',
    realm: 'user',
    username: ''
  };
  private password2;
  private agreement;
  public preferences:any = {background: ''};

  constructor(public navCtrl:NavController,
              public navParams:NavParams,
              public user:User,
              public loadingCtrl:LoadingController,
              private toast:ToastController,
              private api:Api,
                  public dataService: dataService
) {
  }

  ionViewDidLoad() {
    this.getPreferences();
  }

  goBack() {
    this.navCtrl.pop();
  }

  signup(form) {
    this.userInfo.username = this.userInfo.phone;
    let loader = this.loadingCtrl.create({
      content: "Creating your account..."
    });

    //noinspection TypeScriptUnresolvedFunction
    loader.present()
      .then(() => {
      })
      .catch(() => {
        this.showToast(`Please wait...`)
      });

    //_handle error when account created but verification email was not sent

    this.user.signup(this.userInfo)
      .map(res => res.json())
      .subscribe(
        res => {
          //noinspection TypeScriptUnresolvedFunction
          loader.dismiss()
            .catch(() => {
            });
          this.showToast(`Account created successfully!`);
          form.resetForm();
          // this.navCtrl.popTo(this.navCtrl.getByIndex(1));
          this.goToNextState();
        },
        err => {
          //noinspection TypeScriptUnresolvedFunction
          loader.dismiss()
            .catch(() => {
            });
          this.showToast(`Unable to create account! ${err.statusText}`);
          console.error('ERROR', err);
        });
  }
  goToNextState() {
    console.log(this.dataService.currentStateName);
    let length = this.navCtrl.length();
    switch (this.dataService.currentStateName) {
      case 'LessonPage': {
        // Check for subscribe here if not subscribe then go to SubscriptionPage and if already subscribe then go to direct. following condition can be checked.  
          
          this.navCtrl.push(SubscriptionPage);
          this.navCtrl.remove(length-1);
          this.navCtrl.remove(length-2);
        //this.navCtrl.setPages(this.dataService.currentState, this.dataService.currentStateParams);
        break;
      }
      case 'SubscriptionPage': {
        
        // this.navCtrl.push(this.dataService.currentState);
        this.navCtrl.remove(length-1);
        this.navCtrl.remove(length-2);
        //this.navCtrl.setPages(this.dataService.currentState);
        break;
      }
      case 'ProfilePage': {
        
        this.navCtrl.push(this.dataService.currentState);
        this.navCtrl.remove(length-1);
        this.navCtrl.remove(length-2);
        break;
      }
      default: {
        this.navCtrl.setRoot(Home2Page);
      }
    }
  }

  getPreferences() {
    this.api.get(`app_preferences?filter[fields][background]=true`)
      .map(res => res.json())
      .subscribe(res => {
        this.preferences = res[0];
      });
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }
}