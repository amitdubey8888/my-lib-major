import { Component, NgZone } from "@angular/core";
import { NavController, NavParams, ModalController, Platform, ToastController, LoadingController, AlertController } from "ionic-angular";
import { ShareModalPage } from "../../pages/share-modal/share-modal";
import { Api } from "../../providers/api";
import { WindowRef } from "../../providers/window-ref";
import * as _ from 'lodash'
import { User } from "../../providers/user";
import { Network } from '@ionic-native/network';
import { Items } from "../../providers/items";
import { BookPage } from "../book/book";
import { NetworkCheckService } from "../../providers/network.check.service";
import { Settings } from "../../providers/settings";
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { VgAPI } from 'videogular2/core';
import { VideoEditor } from '@ionic-native/video-editor';
import * as firebase from 'firebase';

//noinspection TypeScriptCheckImport
import ImgCache from 'imgcache.js';

@Component({
  selector: 'page-lesson',
  templateUrl: 'lesson.html',
  providers: [WindowRef, VgAPI]
})
export class LessonPage {
  _vgapi: any;
  devHeight: any = {};
  private _window: Window;
  contentScroll: {};
  halfWidth: {};
  devH: number;
  devW: number;
  currentItems: any = [];
  showResults: boolean = true;
  searchResults: {};
  public lsections = [];
  public lesson: any = {
    "lesson_title": "",
    "lesson_icon": "",
    "topicId": ""
  };
  public otherLessons = [];
  public book = {
    "book_title": "",
    "book_description": "",
    "book_thumb": "",
    "book_bg": "",
    "views": null,
    "termsCount": null,
    "termsIcon": "",
  };
  public topic;
  public downloadedData: any = 0;
  connectSubscription: any;
  disconnectSubscription: any;
  //noinspection TypeScriptValidateTypes
  public storageRef = firebase.storage().ref();

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private videoEditor: VideoEditor,
    public vgApi: VgAPI,
    public modalCtrl: ModalController,
    public windowRef: WindowRef,
    private items: Items,
    public toast: ToastController,
    private transfer: Transfer,
    private file: File,
    private ngZone: NgZone,
    private user: User,
    private net: Network,
    private settings: Settings,
    private network: NetworkCheckService,
    public api: Api,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    public _platform: Platform) {
    this._window = windowRef.nativeWindow;
    this.devH = this._window.innerHeight;
    this.devW = this._window.innerWidth;
  }

  onPlayerReady(_vgapi: VgAPI) {
    this._vgapi = _vgapi;
  }
  pause() {
    this._vgapi.pause();
  }

  attachImage(fileUri) {
    // console.log(this.file.applicationDirectory);
    // let directory = fileUri.substring(0, fileUri.lastIndexOf('/') + 1);
    // console.log(directory);
    let filename = fileUri.substring(fileUri.lastIndexOf('/') + 1, fileUri.length)
    console.log(filename);

    // var reader = new FileReader();

    // reader.onloadend = function (e) {
    //   console.log('onloadend()');
    // };

    // reader.readAsDataURL(fileUri);

    // this.file.readAsDataURL(directory, filename).then(blob => {
    // console.log(reader);
    let self = this;
    //noinspection TypeScriptUnresolvedFunction
    this.file.resolveLocalFilesystemUrl(fileUri).then((entry: any) => {
      entry.file(function (file) {
        var reader = new FileReader();

        
        reader.onloadend = (encodedFile: any) =>{
          var src = encodedFile.target.result;
          //noinspection TypeScriptUnresolvedFunction
          src = src.split("base64,");
          var contentAsBase64EncodedString = src[1];
          console.log(encodedFile);
          console.log(contentAsBase64EncodedString);
          self.storageRef.child(`${'lessons/pictures'}/${new Date().getTime() + filename}`).putString(contentAsBase64EncodedString, 'base64')
            .then(snapshot => {
              self.showToast('Image successfully uploaded');
              console.log(snapshot);
              console.log(snapshot.downloadURL);
              let patchObj = {
                "lesson_icon": snapshot.downloadURL
              };
              //noinspection TypeScriptUnresolvedFunction
              self.api.patch('/lessons/' + self.lesson.id, patchObj)
                .map(res => res.json())
                .subscribe((data) => {
                  console.log(data);
                });
            })
            .catch(error => {
              self.showToast('Something bad happened, please retry');
            })
        };
        reader.readAsDataURL(file);
      })
    }).catch((error) => {
      console.log(error);
    });


    // })
  }

  saveThumbnail(fileUri) {
    let loading = this.loadingCtrl.create({
      content: '<div>Saving thumbnail</div>'
    });
    loading.present();
    let filename = 'thumb' + new Date().getTime();
    //noinspection TypeScriptUnresolvedFunction
    this.videoEditor.createThumbnail({
      fileUri: fileUri,
      outputFileName: filename,
      atTime: 2, // optional, location in the video to create the thumbnail (in seconds)
      width: 150, // optional, width of the thumbnail
      height: 100, // optional, height of the thumbnail
      quality: 60 // optional, quality of the thumbnail (between 1 and 100)
    })
      .then((thumbUri: string) => {
        loading.dismiss();
        // thumbUri = thumbUri.substring(thumbUri.indexOf('/0/')+2,thumbUri.length);
        console.log('video thumbnail success ', thumbUri);
        this.attachImage('file://'+thumbUri);
      })
      .catch((error: any) => console.log('video thumbnail error', error));
  }

  ionViewWillEnter() {
    this.setCurrentStyles();
    this.lesson = this.navParams.get('lesson');
    this.book = this.navParams.get('book');
    this.topic = this.navParams.get('topic');
    this.loadImg(this.book.book_bg);

    if (this.network.connected)
      this.collectInfo();
    else
      this.fetchInfo();

    this.connectSubscription = this.net.onConnect()
      .subscribe(() => {
        this.collectInfo();
        this.showToast("Connected to internet");
      });
    this.disconnectSubscription = this.net.onDisconnect()
      .subscribe(() => {
        this.settings.load()
          .then(() => {
            this.fetchInfo();
            this.showToast("Limited connectivity or no internet access!");
          });
      })
  }

  setImg(src) {
    //noinspection TypeScriptUnresolvedFunction
    ImgCache.getCachedFileURL(src, (src, dest: any) => {
      this.book.book_bg = dest;
    });
  }

  loadImg(src) {
    //noinspection TypeScriptUnresolvedFunction
    ImgCache.isCached(src, (path, success) => {
      if (success) {
        this.setImg(src);
      } else {
        //noinspection TypeScriptUnresolvedFunction
        ImgCache.cacheFile(src, (data) => {
          this.setImg(src);
        });
      }
    });
  }

  ionViewWillLeave() {
    this.disconnectSubscription.unsubscribe();
    this.connectSubscription.unsubscribe();
  }

  collectInfo() {
    this.lsections = null;
    this.otherLessons = null;
    this.getLesson();
    this.getLessons();
  }

  fetchInfo() {
    this.lsections = null;
    this.otherLessons = null;
    this.fetchLesson();
    this.fetchLessons();
  }

  setCurrentStyles() {
    this.devHeight = this._platform.height();
    this.contentScroll = { 'overflow-y': 'scroll', 'max-height': (this.devH - 110) + 'px' };
    this.halfWidth = { 'width': ((this.devW - 227) / 2) + 'px' };
    this.searchResults = {
      'width': ((this.devW - 227) / 2) + 'px',
      'max-height': (this.devH - 100) + 'px',
      'overflow-y': 'scroll'
    };
  }

  getLesson() {
    //noinspection TypeScriptUnresolvedFunction
    this.api.get(`lsections?filter={"where": {"lessonId": "${this.lesson.id}"}}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.lsections = response;
        console.log("got sections for ", this.lesson.id, response);
        this.settings.getValue('lsections')
          .then((lsections) => {
            if (!lsections)
              lsections = [];
            let index = _.findIndex(lsections, { 'id': this.lsections[0].id });
            if (index != -1)
              lsections[index] = this.lsections[0];
            else
              lsections.push(this.lsections[0]);

            this.settings.getValue('videos')
              .then(lvideos => {
                console.log("local videos");
                console.log(lvideos);
                _.forEach(lsections, lsection => {
                  let index = _.findIndex(lvideos, { 'remoteURI': lsection.mediaURI });
                  if (index >= 0) {
                    lsection.mediaURI = lvideos[index].localURI
                  }
                });
                let index = _.findIndex(lsections, { 'id': this.lsections[0].id });
                this.lsections[0] = lsections[index];
                console.log("this.lsections", this.lsections)
              });
            this.settings.setValue('lsections', lsections);
            console.log("Local lsections ", lsections);
          })
          .catch(() => {
          });
      });
  }
  getLessons() {
    //noinspection TypeScriptUnresolvedFunction
    this.api.get(`lessons?filter={"where": {"topicId": "${this.topic.id}"}}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.settings.getValue('lessons')
          .then((lessons: any) => {
            if (!lessons)
              lessons = [];

            _.forEach(response, (lesson: any) => {
              let index = _.findIndex(lessons, { 'id': lesson.id });
              if (index != -1) {
                lessons[index] = lesson
              }
              else {
                lessons.push(lesson);
              }
            });
            this.settings.setValue('lessons', lessons);
            console.log("Local lessons ", lessons);
          })
          .catch(() => {
          });
        if (!this.otherLessons)
          this.otherLessons = [];
        this.otherLessons = _.reject(response, { 'id': this.lesson.id })
      });
  }

  fetchLesson() {
    this.settings.getValue('lsections')
      .then((lsections) => {
        console.log(lsections);
        if (!lsections)
          lsections = [];
        lsections = _.filter(lsections, (lsection: any) => {
          return lsection.lessonId == this.lesson.id
        });
        this.settings.getValue('videos')
          .then(lvideos => {
            _.forEach(lsections, lsection => {
              let index = _.findIndex(lvideos, { 'remoteURI': lsection.mediaURI });
              if (index >= 0) {
                lsection.mediaURI = lvideos[index].localURI
              }
            });
            this.lsections = lsections;
            console.log("this.lsections", this.lsections)
          });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  fetchLessons() {
    this.settings.getValue('lessons')
      .then((lessons: any) => {
        console.log(lessons);
        if (!lessons)
          lessons = [];
        lessons = _.filter(lessons, { 'topicId': this.topic.id });
        if (!this.otherLessons)
          this.otherLessons = [];
        this.otherLessons = _.reject(lessons, { 'id': this.lesson.id })
      })
      .catch((error) => {
        console.log(error);
      });
  }

  showLesson(lesson) {
    console.log("acquired", lesson);
    this.lesson = lesson;
    this.topic.id = lesson.topicId;
    if (this.network.connected)
      this.collectInfo();
    else
      this.fetchInfo();
  }

  goBack() {
    this.navCtrl.pop();
  }

  presentModal() {
    let modal = this.modalCtrl.create(ShareModalPage, {
      'lesson': this.lesson,
      'lsections': this.lsections,
      book_bg: this.book.book_bg
    });
    modal.present();
  }

  getItems(ev) {
    this.showResults = true;
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.currentItems = this.items.query({
      name: val
    });
  }

  openItem(item) {
    this.showResults = false;
    if (this.network.connected) {
      switch (item.category) {
        case 'Lesson':
          this.getLessonInfo(item);
          break;
        case 'Book':
          //noinspection TypeScriptUnresolvedFunction
          this.api.get(`books/${item.id}`)
            .map(res => res.json())
            .subscribe((book) => {
              this.navCtrl.push(BookPage, { book: book })
            });
          break;
      }
    }
    else {
      switch (item.category) {
        case 'Lesson':
          this.fetchLessonInfo(item);
          break;
        case 'Book':
          this.settings.getValue('subjects')
            .then((subjects: any) => {
              if (!subjects)
                subjects = [];
              _.forEach(subjects, subject => {
                if (!subject.books)
                  subject.books = [];
                _.forEach(subject.books, book => {
                  if (book.id == item.id)
                    this.navCtrl.push(BookPage, { book: book })
                })
              });
            })
            .catch(() => {
            });
      }
    }
  }

  getLessonInfo(item) {
    let book: any;
    let topic: any;
    let lesson: any = {
      lesson_title: item.name,
      lesson_icon: item.pic,
      id: item.id
    };
    //noinspection TypeScriptUnresolvedFunction
    this.api.get(`lessons/${item.id}/topic`)
      .map(res => res.json())
      .subscribe((response) => {
        topic = response;
        lesson.topicId = topic.id;
        //noinspection TypeScriptUnresolvedFunction
        this.api.get(`books/${topic.bookId}?filter[fields][book_title]=true&filter[fields][book_bg]=true`)
          .map(res => res.json())
          .subscribe((res) => {
            book = res;
            book.id = topic.bookId;
            this.lesson = lesson;
            this.book = book;
            this.topic = topic;
            this.collectInfo();
          })
      });
  }

  fetchLessonInfo(item) {
    let book: any;
    let topic: any;
    let lesson: any = {
      lesson_title: item.name,
      lesson_icon: item.pic,
      id: item.id
    };
    this.settings.getValue('topics')
      .then((topics: any) => {
        _.forEach(topics, ltopic => {
          if (!ltopic.lessons)
            ltopic.lessons = [];
          _.forEach(ltopic.lessons, llesson => {
            if (llesson.id == lesson.id && llesson.topicId == ltopic.id)
              topic = ltopic
          })
        });
        lesson.topicId = topic.id;
        this.settings.getValue('subjects')
          .then((subjects: any) => {
            if (!subjects)
              subjects = [];
            _.forEach(subjects, subject => {
              if (!subject.books)
                subject.books = [];
              _.forEach(subject.books, lbook => {
                if (lbook.id == topic.bookId) {
                  book = lbook;
                  book.id = topic.bookId;
                  this.lesson = lesson;
                  this.book = book;
                  this.topic = topic;
                  this.fetchInfo();
                }
              })
            });
          })
          .catch(() => {
          });
      })
      .catch(() => {
      });
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  saveVideo(section: any, i) {
    console.info("ssss", section.mediaURI.split('///'));
    if (section.mediaURI.split('///')[0] == "file:") {
      console.info("streaming from local");
    }
    else {
      // this.settings.getValue('videos')
      //   .then((lvideos:any) => {
      //     if (!lvideos)
      //       lvideos = [];
      //     let index = _.findIndex(lvideos, {'remoteURI': section.mediaURI});
      //     if (index >= 0) {
      //       console.log("URI is cached", lvideos[index])
      //     }
      //     else {
      //       console.info("not cached, downloading...");
      //       const fileTransfer:TransferObject = this.transfer.create();
      //       const url = section.mediaURI;
      //       section.downloading = 0;
      //       //noinspection TypeScriptUnresolvedFunction
      //       fileTransfer.download(url, this.file.dataDirectory + url, true)
      //         .then((entry) => {
      //           console.info(entry.toURL());
      //           lvideos.push({"remoteURI": url, "localURI": entry.toURL()});
      //           console.log('lvideos', lvideos);
      //           this.settings.setValue('videos', lvideos);
      //           section.downloading = null;
      //         }, (error) => {
      //           section.downloading = null;
      //           console.error("Download Unsuccessful", error);
      //           // handle error
      //         });
      //       fileTransfer.onProgress(listener => {
      //         this.ngZone.run(() => {
      //           section.downloading = Math.floor((listener.loaded / listener.total) * 100);
      //         })
      //       })
      //     }
      //   })
    }
  }
  videoEnd() {
    console.log('video ended');
  }
  downLoadOffline(section) {
    console.log('down load video offline');
    console.info("ssss", section.mediaURI.split('///'));
    if (section.mediaURI.split('///')[0] == "file:") {
      console.log("Already offline");
      this.showToast('Video is alredy available offline');
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Confirm Download',
        message: 'Do you want to download this video offline?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Download',
            handler: () => {
              console.log('Download clicked');
              let loading = this.loadingCtrl.create({
                content: '<div>Downloading ' + this.downloadedData + '%</div>'
              });

              loading.present();
              this.settings.getValue('videos')
                .then((lvideos: any) => {
                  if (!lvideos)
                    lvideos = [];
                  let index = _.findIndex(lvideos, { 'remoteURI': section.mediaURI });
                  if (index >= 0) {
                    console.log("URI is cached", lvideos[index])
                  }
                  else {
                    console.info("not cached, downloading...");
                    const fileTransfer: TransferObject = this.transfer.create();
                    const url = section.mediaURI;
                    section.downloading = 0;
                    this.pause();
                    //noinspection TypeScriptUnresolvedFunction
                    fileTransfer.download(url, this.file.dataDirectory + url, true)
                      .then((entry) => {
                        console.info(entry.toURL());
                        lvideos.push({ "remoteURI": url, "localURI": entry.toURL() });
                        console.log('lvideos', lvideos);
                        this.saveThumbnail(entry.toURL());
                        section.mediaURI = entry.toURL();
                        this.settings.setValue('videos', lvideos);
                        section.downloading = null;
                        loading.setContent('Downloading 100%');
                        loading.dismiss();
                        this.downloadedData = 0;
                      }, (error) => {
                        section.downloading = null;
                        console.error("Download Unsuccessful", error);
                        loading.dismiss();
                        this.downloadedData = 0;
                        // handle error
                      });
                    fileTransfer.onProgress(listener => {
                      this.ngZone.run(() => {
                        section.downloading = Math.floor((listener.loaded / listener.total) * 100);
                        this.downloadedData = Math.floor((listener.loaded / listener.total) * 100);
                        loading.setContent('Downloading ' + this.downloadedData + '%');
                        console.log(this.downloadedData);
                      })
                    })
                  }
                })
            }
          }
        ]
      });
      alert.present();

    }

  }
}