// import { SigninPage } from './signin/signin';
import { Home2Page } from './home2/home2';

// import { SearchPage } from './search/search';
// import { SettingsPage } from './settings/settings';

// The page the user lands on after opening the app and without a session
export const FirstRunPage = Home2Page;

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = Home2Page;

