import {Component} from "@angular/core";
import {NavController, NavParams, Platform, ToastController, LoadingController} from "ionic-angular";
import {WindowRef} from "../../providers/window-ref";
import {LessonPage} from "../../pages/lesson/lesson";
import {Api} from "../../providers/api";
import * as _ from "lodash";
import {User} from "../../providers/user";
import {Network} from '@ionic-native/network';
import {Items} from "../../providers/items";
import {Settings} from "../../providers/settings";
import {dataService} from "../../providers/data.service";
import {SigninPage} from "../signin/signin";
import {SubscriptionPage} from "../subscription/subscription";
import {NetworkCheckService} from "../../providers/network.check.service";

//noinspection TypeScriptCheckImport
import ImgCache from 'imgcache.js';

@Component({
  selector: 'page-book',
  templateUrl: 'book.html',
  providers: [WindowRef],
})

export class BookPage {
  devHeight:any = {};
  private _window:Window;
  devH:number;
  devW:number;
  halfWidth:{};
  contentHeight:{};
  searchResults:{};
  currentItems:any = [];
  showResults:boolean = false;
  localbg:any;
  private book = {
    "book_title": "",
    "book_description": "",
    "book_cover": "",
    "book_thumb": "",
    "book_bg": "",
    "views": null,
    "termsCount": null,
    "termsIcon": "",
    "archived": false,
    "id": "",
    "subjectId": ""
  };
  private subject:any = {};
  private topics = [
    {
      topic_term: null,
      topic_name: '',
      lessons: [{
        lesson_title: ''
      }]
    }
  ];
  private topicsCount = null;
  private lessonsCount = null;
  private tt = [];
  connectSubscription:any;
  disconnectSubscription:any;
  private loader = this.loadingCtrl.create({
    content: "loading...",
  });

  constructor(public navCtrl:NavController,
              public navParams:NavParams,
              public windowRef:WindowRef,
              private api:Api,
              private items:Items,
              private loadingCtrl:LoadingController,
              private network:NetworkCheckService,
              private settings:Settings,
              public toast:ToastController,
              private user:User,
              private net:Network,
              public dataService : dataService,
              public _platform:Platform) {
    this._window = windowRef.nativeWindow;
    this.devH = this._window.innerHeight;
    this.devW = this._window.innerWidth;
  }

  ionViewWillEnter() {
    this.setCurrentStyles();
    this.book = this.navParams.get('book');
    console.log("book", this.book);
    this.loadImg(this.book.book_bg);

    if (this.network.connected)
      this.collectInfo();
    else
      this.fetchInfo();

    this.connectSubscription = this.net.onConnect()
      .subscribe(() => {
        this.collectInfo();
        this.showToast("Connected to internet");
      });
    this.disconnectSubscription = this.net.onDisconnect()
      .subscribe(() => {
        this.settings.load()
          .then(() => {
            this.fetchInfo();
            this.showToast("Limited connectivity or no internet access!");
          });
      })
  }

  setImg(src) {
    //noinspection TypeScriptUnresolvedFunction
    ImgCache.getCachedFileURL(src, (src, dest:any) => {
      this.localbg = dest;
      console.log(this.localbg);
    });
  }

  loadImg(src) {
    //noinspection TypeScriptUnresolvedFunction
    ImgCache.isCached(src, (path, success) => {
      console.log(path, success);
       console.log(src);
      if (success) {
        this.setImg(src);
      } else {
        //noinspection TypeScriptUnresolvedFunction
        ImgCache.cacheFile(src, (data) => {
          console.log(src);
          this.setImg(src);
        },
        err=>console.log(err));
      }
    });
  }

  ionViewWillLeave() {
    this.disconnectSubscription.unsubscribe();
    this.connectSubscription.unsubscribe();
  }

  collectInfo() {
    console.log('collect');
    this.topics = null;
    this.lessonsCount = null;
    this.tt = null;
    this.getTopics();
    this.countTopics();
    this.checkUserHasRead();
    this.getSubjectInfo();
  }

  fetchInfo() {
    console.log('fetch');
    this.topics = null;
    this.lessonsCount = null;
    this.tt = null;
    this.fetchTopics();
    this.fetchSubjectInfo();
  }

  setCurrentStyles() {
    this.devHeight = this._platform.height();
    this.halfWidth = {'width': ((this.devW - 227) / 2) + 'px'};
    this.searchResults = {
      'width': ((this.devW - 227) / 2) + 'px',
      'max-height': (this.devH - 100) + 'px',
      'overflow-y': 'scroll'
    };
    this.contentHeight = {'max-height': (this.devH - 100) + 'px', 'overflow-y': 'scroll'};
  }

  goBack() {
    this.navCtrl.pop();
  }

  openLesson(lesson, topic, book) {
    if (!this.user.currentUser) {
      this.dataService.currentStateName = 'LessonPage';
      this.dataService.currentState = LessonPage;
      this.dataService.currentStateParams = {lesson: lesson, book: book || this.book, topic: topic};
      this.navCtrl.push(SigninPage);
      this.showToast("You must be logged in to continue")
    }
    else if (!this.user.currentUser.subscribed_plan) {
      this.dataService.currentStateName = 'LessonPage';
      this.dataService.currentState = LessonPage;
      this.dataService.currentStateParams = {lesson: lesson, book: book || this.book, topic: topic};
      this.navCtrl.push(SubscriptionPage);
      this.showToast("Your need a subscription to continue")
    }
    else {
      let today = new Date();
      //noinspection TypeScriptValidateTypes,TypeScriptUnresolvedFunction
      let endDate = new Date(new Date(this.user.currentUser.subscribed_plan.subscriptionDate).getTime() + this.user.currentUser.subscribed_plan.subscription_duration * 24 * 3600 * 1000);
      if (endDate < today) {
        this.dataService.currentStateName = 'LessonPage';
        this.dataService.currentState = LessonPage;
        this.dataService.currentStateParams = {lesson: lesson, book: book || this.book, topic: topic};
        this.navCtrl.push(SubscriptionPage);
        this.showToast("Your subscription has ended, Renew your subscription to continue")
      }
      else {
        this.navCtrl.push(LessonPage, {lesson: lesson, book: book || this.book, topic: topic});
      }
    }
  }

  getTopics() {
    //noinspection TypeScriptUnresolvedFunction
    this.loader.present()
      .then(() => {
      })
      .catch(() => {
        this.showToast(`Please wait...`);
      });
    this.api.get(`topics?filter={"where": {"bookId": "${this.book.id}"}, "include": {"relation": "lessons"}}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.topics = response;
        this.settings.getValue('topics')
          .then((topics) => {
            if (!topics)
              topics = [];
            _.forEach(this.topics, (topic:any) => {
              let index = _.findIndex(topics, {'id': topic.id});
              if (index != -1) {
                topics[index] = topic
              }
              else {
                topics.push(topic);
              }
            });
            this.settings.setValue('topics', topics);
            console.log("Local topics ", topics)
          })
          .catch(() => {
          });
        this.lessonsCount = response.map((topic:any) => topic.lessons.length).reduce((sum, n) => sum + n, 0);
        let tt = _.groupBy(this.topics, 'topic_term');
        _.forEach(tt, (value, key) => {
          if (!this.tt)
            this.tt = [];
          this.tt.push({number: key, topics: value});

          //noinspection TypeScriptUnresolvedFunction
          this.loader.dismiss()
            .catch(() => {})
        });
      }, error => {
        this.fetchTopics();
      });
  }

  fetchTopics() {
    this.settings.getValue('topics')
      .then((topics) => {
        if (!topics)
          topics = [];
        topics = _.filter(topics, (topic:any) => {
          return topic.bookId == this.book.id
        });
        this.topics = topics;
        this.countTopicsOffline();
        this.lessonsCount = topics.map((topic:any) => topic.lessons.length).reduce((sum, n) => sum + n, 0);
        let tt = _.groupBy(this.topics, 'topic_term');
        _.forEach(tt, (value, key) => {
          if (!this.tt)
            this.tt = [];
          this.tt.push({number: key, topics: value})
        });
      })
      .catch(() => {
      })
  }

  countTopics() {
    this.api.get(`topics/count?where={"bookId":"${this.book.id}"}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.topicsCount = response.count;
      });
  }

  countTopicsOffline() {
    this.topicsCount = this.topics.map((topic:any) => {
      return topic.bookId == this.book.id ? 1 : 0
    }).reduce((sum, n) => sum + n, 0)
  }

  checkUserHasRead() {
    if (this.user.currentUser) {
      if (this.user.currentUser.has_read && this.user.currentUser.has_read.length) {
        if (!_.find(this.user.currentUser.has_read, (val) => {
            return val == this.book.id
          })) {
          this.user.currentUser.has_read.push(this.book.id);
          this.updateUser();
          this.updateBookViews();
        }
      } else {
        this.user.currentUser.has_read = [];
        this.user.currentUser.has_read.push(this.book.id);
        this.updateUser();
        this.updateBookViews();
      }
    }
  }

  updateBookViews() {
    this.book.views++;
    this.api.patch('books/' + this.book.id, this.book)
      .map(res => res.json())
      .subscribe(() => {
      });
  }

  updateUser() {
    this.api.put("users/updateUser", {details: this.user.currentUser})
      .map(res => res.json())
      .subscribe(() => {
        this.settings.setValue('profile', this.user.currentUser);
      });
  }

  getItems(ev) {
    this.showResults = true;
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.currentItems = this.items.query({
      name: val
    });
  }

  openItem(item) {
    this.showResults = false;
    if (this.network.connected) {
      switch (item.category) {
        case 'Lesson':
          this.getLessonInfo(item);
          break;
        case 'Book':
          this.book = item;
          this.collectInfo();
          this.api.get(`books/${this.book.id}`)
            .map(res => res.json())
            .subscribe((response) => {
              this.book = response
            });
          break;
      }
    }
    else {
      switch (item.category) {
        case 'Lesson':
          this.fetchLessonInfo(item);
          break;
        case 'Book':
          this.book = item;
          this.fetchInfo();
          this.settings.getValue('subjects')
            .then((subjects:any) => {
              if (!subjects)
                subjects = [];
              _.forEach(subjects, subject => {
                if (!subject.books)
                  subject.books = [];
                _.forEach(subject.books, book => {
                  if (book.id == item.id)
                    this.book = book;
                })
              });
            })
            .catch(() => {
            });
          break;
      }
    }
  }

  getLessonInfo(item) {
    let that = this;
    let book:any;
    let topic:any;
    let lesson:any = {
      lesson_title: item.name,
      lesson_icon: item.pic,
      id: item.id
    };
    this.api.get(`lessons/${item.id}/topic`)
      .map(res => res.json())
      .subscribe((response) => {
        topic = response;
        lesson.topicId = topic.id;
        that.api.get(`books/${topic.bookId}?filter[fields][book_title]=true&filter[fields][book_bg]=true`)
          .map(res => res.json())
          .subscribe((res) => {
            book = res;
            book.id = topic.bookId;
            this.openLesson(lesson, topic, book);
          })
      });
  }

  fetchLessonInfo(item) {
    let book:any;
    let topic:any;
    let lesson:any = {
      lesson_title: item.name,
      lesson_icon: item.pic,
      id: item.id
    };
    this.settings.getValue('topics')
      .then((topics:any) => {
        _.forEach(topics, ltopic => {
          if (!ltopic.lessons)
            ltopic.lessons = [];
          _.forEach(ltopic.lessons, llesson => {
            if (llesson.id == lesson.id && llesson.topicId == ltopic.id)
              topic = ltopic
          })
        });
        lesson.topicId = topic.id;
        this.settings.getValue('subjects')
          .then((subjects:any) => {
            if (!subjects)
              subjects = [];
            _.forEach(subjects, subject => {
              if (!subject.books)
                subject.books = [];
              _.forEach(subject.books, lbook => {
                if (lbook.id == topic.bookId) {
                  book = lbook;
                  book.id = topic.bookId;
                  this.openLesson(lesson, topic, book);
                }
              })
            });
          })
          .catch(() => {
          });
      })
      .catch(() => {
      });
  }

  getSubjectInfo() {
    this.api.get(`subjects/${this.book.subjectId}`)
      .map(res => res.json())
      .subscribe((response) => {
        this.subject = response;
      });
  }

  fetchSubjectInfo() {
    this.settings.getValue('subjects')
      .then((subjects:any) => {
        if (!subjects)
          subjects = [];
        this.subject = subjects.find(subject => {
          return subject.id == this.book.subjectId
        });
      })
      .catch(() => {
      });
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
}