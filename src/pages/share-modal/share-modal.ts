import {Component} from '@angular/core';
import {NavController, NavParams, ViewController} from 'ionic-angular';
import {WindowRef} from "../../providers/window-ref";
import {SocialSharing} from '@ionic-native/social-sharing';

//noinspection TypeScriptCheckImport
import ImgCache from 'imgcache.js';
@Component({
  selector: 'page-share-modal',
  templateUrl: 'share-modal.html'
})
export class ShareModalPage {
  public lesson:{
    lesson_title:''
  };
  public lsections:any = [];
  private _window:Window;
  contentScroll:{};
  cardHeight:{};
  devH:number;
  devW:number;
  public book_bg:string;

  constructor(public navCtrl:NavController,
              public navParams:NavParams,
              public windowRef:WindowRef,
              private socialSharing:SocialSharing,
              public viewCtrl:ViewController) {
    this._window = windowRef.nativeWindow;
    this.devH = this._window.innerHeight;
    this.devW = this._window.innerWidth;
  }

  setCurrentStyles() {
    this.cardHeight = {'max-height': (this.devH - 100) + 'px'};
    this.contentScroll = {'overflow-y': 'scroll', 'max-height': (this.devH - 172) + 'px'};
    this.loadImg(this.book_bg);
  }

  setImg(src) {
    //noinspection TypeScriptUnresolvedFunction
    ImgCache.getCachedFileURL(src, (src, dest:any) => {
      this.book_bg = dest;
    });
  }

  loadImg(src) {
    //noinspection TypeScriptUnresolvedVariable,TypeScriptUnresolvedFunction
    ImgCache.isCached(src, (path, success) => {
      if (success) {
        this.setImg(src);
      } else {
        //noinspection TypeScriptUnresolvedFunction
        ImgCache.cacheFile(src, (data) => {
          this.setImg(src);
        });
      }
    });
  }

  ionViewDidLoad() {
    this.setCurrentStyles();
    this.lesson = this.navParams.get('lesson');
    this.lsections = this.navParams.get('lsections');
    this.book_bg = this.navParams.get('book_bg');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  openSharingOpts() {
    let description = "";
    let imageURI = [];
    let that = this;

    function callback() {
      console.log('all done');
      that.socialSharing.share(description, that.lesson.lesson_title, imageURI || null, null);
    }

    var itemsProcessed = 0;
    this.lsections.forEach((section, index, array) => {
      description += section.section_description + '\n\n';
      if (!section.imageURI) {
        itemsProcessed++;
        if (itemsProcessed === array.length) {
          callback();
        }
      }
      else {
        this.convertToDataURLviaCanvas(section.imageURI, "image/png", (base64Image) => {
          itemsProcessed++;
          imageURI.push(base64Image);
          if (itemsProcessed === array.length) {
            callback();
          }
        });
      }
    });
  }

  convertToDataURLviaCanvas(url, outputFormat, cb) {
    console.log("in conversion");
    //noinspection TypeScriptUnresolvedFunction
    return new Promise((resolve, reject) => {
      let img = new Image();
      img.crossOrigin = 'Anonymous';
      img.onload = function () {
        let canvas = <HTMLCanvasElement> document.createElement('CANVAS'),
          ctx = canvas.getContext('2d'),
          dataURL;
        canvas.height = this.height;
        canvas.width = this.width;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        canvas = null;
        cb(dataURL);
        resolve(dataURL);
      };
      img.src = url;
    });
  }
}
