import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { User } from '../../providers/user';
import { Api } from "../../providers/api";
import { dataService } from "../../providers/data.service";

@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html'
})

export class ResetPasswordPage implements OnInit {
  public resetForm: any;
  public loading: any;
  public preferences: any = { background: '' }

  constructor(public navCtrl: NavController,
    public dataService: dataService,
    public navParams: NavParams,
    public userLogin: User,
    public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController,
    private toastCtrl: ToastController,
    public api: Api) {
    this.resetForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, EmailValidator.isValid])],
    });
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    })
  }

  ngOnInit() {
    this.getPreferences();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }

  send(email: string): void {
    this.loading.present();
    console.log(email);
    this.userLogin.resetPassword(email)
      .map(res => res.json())
      .subscribe(
      data => {
        let successToast = this.toastCtrl.create({
          message: 'Check your inbox for further instructions!',
          duration: 5000,
          position: 'bottom'
        });
        console.info("res", data);
        this.loading.dismiss();
        successToast.present();
        this.navCtrl.pop();
      },
      error => {
        console.warn("Error", error);
        this.loading.dismiss();
        let errorToast = this.toastCtrl.create({
          message: error.statusText,
          duration: 5000,
          position: 'bottom'
        });
        errorToast.present();
      }
      )
  }

  goBack() {
    this.navCtrl.pop();
  }

  getPreferences() {
    // this.api.get(`app_preferences?filter[fields][background]=true`)
    //   .map(res => res.json())
    //   .subscribe(res => {
    //     this.preferences = res[0];
    //   });
    this.preferences.background = this.dataService.homeBgPath;
  }
}
