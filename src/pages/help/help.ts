import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { Api } from "../../providers/api";
import { dataService } from "../../providers/data.service";

@Component({
  selector: 'page-help',
  templateUrl: 'help.html'
})
export class HelpPage implements OnInit {
  devHeight: any = {};
  contentHeight:any = {};
  public preferences: any = { background: '' };

  constructor(public navCtrl: NavController,
    public dataService: dataService,
    public navParams: NavParams,
    private api: Api,
    public _platform: Platform) {
  }

  ngOnInit() {
    this.getPreferences();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HelpPage');
    this.devHeight = this._platform.height();
    this.contentHeight = {'max-height': (this.devHeight - 110) + 'px', 'overflow-y': 'scroll'};
  }

  goBack() {
    this.navCtrl.pop();
  }

  getPreferences() {
    this.api.get(`app_preferences?filter[fields][help]=true`)
      .map(res => res.json())
      .subscribe(res => {
        this.preferences = res[0];
      });
    this.preferences.background = this.dataService.homeBgPath;
  }
}