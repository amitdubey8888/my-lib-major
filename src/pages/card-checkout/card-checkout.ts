import {Component, OnInit} from '@angular/core';
import {NavController, NavParams, LoadingController, Platform, ToastController} from 'ionic-angular';
import {Api} from "../../providers/api";
import {Stripe} from '@ionic-native/stripe';
import {WindowRef} from "../../providers/window-ref";
import {User} from "../../providers/user";
import {SigninPage} from "../signin/signin";
import {Home2Page} from "../home2/home2";
import {Settings} from "../../providers/settings";
import { dataService } from "../../providers/data.service";

@Component({
  selector: 'page-card-checkout',
  templateUrl: 'card-checkout.html',
  providers: [WindowRef]
})
export class CardCheckoutPage implements OnInit {

  public preferences:any = {background: ''};
  public plan:any;
  private card = {
    number: '4242424242424242',
    expMonth: 11,
    expYear: 2018,
    cvc: '222'
  };
  private loader = this.loadingCtrl.create({
    content: "Processing your payment...",
    dismissOnPageChange: true
  });
  private email:string = this.user.currentUser.email;

  constructor(public navCtrl:NavController,
              public navParams:NavParams,
              public loadingCtrl:LoadingController,
              public dataService: dataService,
              private stripe:Stripe,
              private plt:Platform,
              private toast:ToastController,
              private settings:Settings,
              public window:WindowRef,
              private api:Api,
              private user:User) {
  }

  ngOnInit() {
    if (!this.user.currentUser) {
      this.navCtrl.push(SigninPage);
    } else {
      this.getPreferences();
      this.plan = this.navParams.get('plan');
      this.stripe.setPublishableKey('pk_test_nucxaHlNBFh2GDWoTvwNf38L');
    }
  }

  checkout(form) {
    if (this.plan.subscription_amount > 0) {
      this.loader.present();
      //noinspection TypeScriptUnresolvedFunction
      this.stripe.createCardToken(this.card)
        .then(token => {
          console.log(token);
          let payload = {
            "stripeToken": token,
            "email": this.email,
            "amount": this.plan.subscription_amount * 100,
            "description": `Subscription ${this.plan.subscription_title}`,
            "currency": 'ugx'
          };

          this.api.pay('charge', payload)
            .map(res => res.json())
            .subscribe(res => {
              console.info("resp", res);
              if (res.status == 'succeeded') {
                this.handleSuccess();
                form.resetForm();
              }
              else {
                //_handle err
              }
            }, err => console.warn("err", err))
        })
        .catch(error => console.error(error));
    } else {
      //_handle case. However it's least likely to have such a case
    }
  }

  goBack() {
    this.navCtrl.pop();
  }

  getPreferences() {
    // this.api.get(`app_preferences?filter[fields][background]=true`)
    //   .map(res => res.json())
    //   .subscribe(res => {
    //     this.preferences = res[0];
    //   });

    this.preferences.background = this.dataService.homeBgPath;
  }

  handleSuccess() {
    this.plan.subscriptionDate = new Date();
    this.user.currentUser.subscribed_plan = this.plan;
    this.api.put("users/updateUser", {details: this.user.currentUser})
      .map(res => res.json())
      .subscribe(
        response => {
          this.settings.setValue('profile', this.user.currentUser);
          this.loader.dismiss();
          let goTo:any = Home2Page;
          if(this.dataService.currentState) {
            goTo = this.dataService.currentState;
          }
          this.navCtrl.setRoot(goTo);
          this.dataService.currentState = null;
          this.showToast(`Successfully subscribed to ${this.plan.subscription_title} plan`)
        },
        error => {
          console.log(error);
          //_handle network check and timeout
          this.handleSuccess();
        }
      );
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }
}