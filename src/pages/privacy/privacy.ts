import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { Api } from "../../providers/api";
import { dataService } from "../../providers/data.service";

/*
 Generated class for the Privacy page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-privacy',
  templateUrl: 'privacy.html'
})
export class PrivacyPage implements OnInit {
  devHeight: any = {};
  public preferences: any = { background: '' }

  constructor(public navCtrl: NavController,
    public dataService: dataService,
    public navParams: NavParams,
    private api: Api,
    public _platform: Platform) {
  }

  ngOnInit() {
    this.devHeight = this._platform.height();
    this.getPreferences();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrivacyPage');
  }

  goBack() {
    this.navCtrl.pop();
  }

  getPreferences() {
    this.api.get(`app_preferences?filter[fields][background]=true&filter[fields][privacy_policy]=true`)
      .map(res => res.json())
      .subscribe(res => {
        this.preferences = res[0];
      });

    // this.preferences.background = this.dataService.homeBgPath;
  }
}
