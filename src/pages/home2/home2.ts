import { Component, ElementRef } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, ToastController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { BookPage } from '../../pages/book/book';
import { Api } from '../../providers/api';
import { WindowRef } from "../../providers/window-ref";
import { Settings } from "../../providers/settings";
import { Items } from "../../providers/items";
import { Network } from '@ionic-native/network';
import { NetworkCheckService } from "../../providers/network.check.service";
import { User } from "../../providers/user";
import { SigninPage } from "../signin/signin";
import { SubscriptionPage } from "../subscription/subscription";
import { LessonPage } from "../lesson/lesson";
import * as _ from 'lodash';
import { dataService } from "../../providers/data.service";
//noinspection TypeScriptCheckImport
import ImgCache from 'imgcache.js';

@Component({
  selector: 'page-home2',
  templateUrl: 'home2.html'
})

export class Home2Page {
  devHeight: any = {};
  halfWidth: {};
  fullWidth: {};
  fullHeight: {};
  backdrop: {};
  devH: number;
  randomHome: any;
  searchOpen: Boolean;
  devW: number;
  private _window: Window;
  contentHeight: {};
  currentItems: any = [];
  searchResults: {};
  showResults: boolean = false;
  preferences: any = { background: '' };
  private subjects = [];
  private loader = this.loadingCtrl.create({
    content: "Loading...",
  });
  private caching = this.loadingCtrl.create({
    content: "Caching...",
  });
  connectSubscription: any;
  disconnectSubscription: any;

  constructor(public navCtrl: NavController,
    public dataService: dataService,
    public navParams: NavParams,
    private sanitizer: DomSanitizer,
    private api: Api,
    private items: Items,
    private loadingCtrl: LoadingController,
    private settings: Settings,
    public windowRef: WindowRef,
    public toast: ToastController,
    private user: User,
    private net: Network,
    private network: NetworkCheckService,
    public el: ElementRef,
    public _platform: Platform) {
    this._window = windowRef.nativeWindow;
    this.devH = this._window.innerHeight;
    this.devW = this._window.innerWidth;
  }

  ionViewWillEnter() {
    this.setCurrentStyles();
    if (this.network.connected) {
      this.settings.load()
        .then(() => {
          this.getPreferences();
          this.getSubjects();
        })
    }
    else {
      this.settings.load()
        .then(() => {
          this.fetchPreferences();
          this.fetchSubjects();
        });
    }

    this.connectSubscription = this.net.onConnect()
      .subscribe(() => {
        this.settings.load()
          .then(() => {
            this.getPreferences();
            this.getSubjects();
            this.showToast("Connected to internet");
          })
      });
    this.disconnectSubscription = this.net.onDisconnect()
      .subscribe(() => {
        this.settings.load()
          .then(() => {
            this.fetchPreferences();
            this.fetchSubjects();
            this.showToast("Limited connectivity or no internet access!");
          });
      })
  }

  ionViewWillLeave() {
    this.disconnectSubscription.unsubscribe();
    this.connectSubscription.unsubscribe();
  }

  setCurrentStyles() {
    this.devHeight = this._platform.height();
    this.halfWidth = { 'width': ((this.devW - 227) / 2) + 'px' };
    this.fullWidth = { 'width': this.devW * 0.6 + 'px' };
    this.fullHeight = { 'height': 60 + 'px' };

    this.backdrop = { 'width': this._platform.width() + 'px', 'height': this._platform.height() + 'px' };

    this.searchResults = {
      'width': { 'width': this.devW * 0.6 + 'px' },
      'max-height': (this.devH - 100) + 'px',
      'overflow-y': 'scroll'
    };
  }

  openBook(book) {
    this.navCtrl.push(BookPage, { book: book });
  }

  getImageUrl(imageUrl: any) {
    return this.sanitizer.bypassSecurityTrustUrl(imageUrl);
  }

  processBook(subjectIndex, bookIndex) {
    console.log('book index ' + bookIndex);
    console.log('subject index ' + subjectIndex);

    if (bookIndex < this.subjects[subjectIndex].books.length) {
      let src = this.subjects[subjectIndex].books[bookIndex].book_thumb;
      ImgCache.isCached(src, (path, success) => {
        if (success) {
          //noinspection TypeScriptUnresolvedFunction
          ImgCache.getCachedFileURL(src, (src, dest: any) => {
            console.log(dest);
            // this.subjects[subjectIndex].books[bookIndex].book_thumb = this.getImageUrl(dest);
            console.log('book changed from ' + bookIndex)
            this.processBook(subjectIndex, bookIndex + 1);
          },
            err => console.log(err));
        } else {
          //noinspection TypeScriptUnresolvedFunction
          ImgCache.cacheFile(src, (data) => {
            //noinspection TypeScriptUnresolvedFunction
            ImgCache.getCachedFileURL(src, (src, dest: any) => {
              console.log(dest);
              // this.subjects[subjectIndex].books[bookIndex].book_thumb = this.getImageUrl(dest);
              console.log('book changed from ' + bookIndex)
              this.processBook(subjectIndex, bookIndex + 1);
            },
              err => console.log(err));
          });
        }
      })
    }
    else {
      console.log('subject changed from ' + subjectIndex)
      this.processSubject(subjectIndex + 1, 0);
    }
  }

  processSubject(subjectIndex, bookIndex) {
    if (subjectIndex < this.subjects.length) {
      this.processBook(subjectIndex, bookIndex);
    }
    else {
      //When all have been processed
      this.settings.setValue('subjects', this.subjects);
      this.caching.dismiss();
      this.dataService.home_caching_status = true;
    }
  }


  getSubjects() {
    //noinspection TypeScriptUnresolvedFunction
    this.loader.present()
      .then(() => {
      })
      .catch(() => {
        this.showToast(`Please wait...`);
      });
    this.api.get('subjects?filter[include]=books')
      .map(res => res.json())
      .subscribe((response) => {
        //noinspection TypeScriptUnresolvedFunction
        console.log(response);
        this.loader.dismiss()
          .catch(() => {
          });
        this.subjects = response;

        
        if (!this.dataService.home_caching_status) {
          this.caching.present();
          this.processSubject(0, 0);
        }

      }
      // this.settings.setValue('subjects', this.subjects);

      , (error) => {
        //noinspection TypeScriptUnresolvedFunction
        this.loader.dismiss()
          .catch(() => {
          });
        this.fetchSubjects();
      });
  }


  getPreferences() {
    //noinspection TypeScriptUnresolvedFunction
    this.loader.present()
      .then(() => {
      })
      .catch(() => {
        // this.showToast(`Please wait...`);
      });
    this.api.get(`app_preferences?filter[fields][background]=true`)
      .map(res => res.json())
      .subscribe(
      res => {
        //noinspection TypeScriptUnresolvedFunction
        this.loader.dismiss()
          .catch(() => {
          });
        this.preferences = res[0];
        this.settings.getValue('preferences')
          .then((preferences) => {
            if (!preferences)
              preferences = {};
            let src = res[0].background;
            console.log(src);
            //noinspection TypeScriptUnresolvedFunction
            ImgCache.isCached(src, (path, success) => {
              if (success) {
                //noinspection TypeScriptUnresolvedFunction
                ImgCache.getCachedFileURL(src, (src, dest: any) => {
                  this.preferences.background = dest;
                  preferences.background = this.preferences.background;
                  this.settings.setValue('preferences', preferences);
                  this.dataService.homeBgPath = preferences.background;
                });
              } else {
                //noinspection TypeScriptUnresolvedFunction
                ImgCache.cacheFile(src, (data) => {
                  //noinspection TypeScriptUnresolvedFunction
                  ImgCache.getCachedFileURL(src, (src, dest: any) => {
                    console.log(dest);
                    this.preferences.background = dest;
                    preferences.background = this.preferences.background;
                    this.settings.setValue('preferences', preferences);
                    this.dataService.homeBgPath = preferences.background;
                  });
                });
              }
            });
          })
          .catch(error => {
          })
      },
      error => {
        //noinspection TypeScriptUnresolvedFunction
        this.loader.dismiss()
          .catch(() => {
          });
        this.fetchPreferences();
      });
  }

  fetchSubjects() {
    this.settings.getValue('subjects')
      .then((subjects) => {
        if (!subjects)
          subjects = [];
        this.subjects = subjects;
        console.info("Subjects", subjects)
      })
      .catch(error => {
      })
  }

  fetchPreferences() {
    this.settings.getValue('preferences')
      .then((preferences) => {
        if (!preferences)
          preferences = {};
        this.preferences = preferences;
      })
      .catch(error => {
      })
  }

  getItems(ev) {
    this.showResults = true;
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.currentItems = this.items.query({
      name: val
    });
  }

  openLesson(lesson, topic, book) {
    if (!this.user.currentUser) {
      this.navCtrl.push(SigninPage);
      this.showToast("You must be logged in to continue")
    }
    else if (!this.user.currentUser.subscribed_plan) {
      this.navCtrl.push(SubscriptionPage);
      this.showToast("Your need a subscription to continue")
    }
    else {
      let today = new Date();
      //noinspection TypeScriptValidateTypes,TypeScriptUnresolvedFunction
      let endDate = new Date(new Date(this.user.currentUser.subscribed_plan.subscriptionDate).getTime() + this.user.currentUser.subscribed_plan.subscription_duration * 24 * 3600 * 1000);
      if (endDate < today) {
        this.navCtrl.push(SubscriptionPage);
        this.showToast("Your subscription has ended, Renew your subscription to continue")
      }
      else {
        this.navCtrl.push(LessonPage, { lesson: lesson, book: book, topic: topic });
      }
    }
  }

  toggleSearch() {
    this.searchOpen = !this.searchOpen;
    if (this.searchOpen) {
      this.fullHeight = { 'height': this.devH * 0.9 + 'px' };
    }
    else {
      this.fullHeight = { 'height': 60 + 'px' };
    }
  }
  closeSearch() {
    this.searchOpen = false;
    this.showResults = false;
  }

  openItem(item) {
    this.randomHome = '';
    this.showResults = false;
    // this.searchOpen = false;
    // this.fullHeight = { 'height': 60 + 'px' };

    if (this.network.connected) {
      switch (item.category) {
        case 'Lesson':
          this.getLessonInfo(item);
          break;
        case 'Book':
          this.api.get(`books/${item.id}`)
            .map(res => res.json())
            .subscribe((book) => {
              this.openBook(book)
            });
          break;
      }
    }
    else {
      switch (item.category) {
        case 'Lesson':
          this.fetchLessonInfo(item);
          break;
        case 'Book':
          this.fetchBookInfo(item);
          break;
      }
    }
  }

  getLessonInfo(item) {
    let book: any;
    let topic: any;
    let lesson: any = {
      lesson_title: item.name,
      lesson_icon: item.pic,
      id: item.id
    };
    this.api.get(`lessons/${item.id}/topic`)
      .map(res => res.json())
      .subscribe((response) => {
        topic = response;
        lesson.topicId = topic.id;
        this.api.get(`books/${topic.bookId}?filter[fields][book_title]=true&filter[fields][book_bg]=true`)
          .map(res => res.json())
          .subscribe((res) => {
            book = res;
            book.id = topic.bookId;
            this.openLesson(lesson, topic, book);
          })
      });
  }

  fetchLessonInfo(item) {
    let book: any;
    let topic: any;
    let lesson: any = {
      lesson_title: item.name,
      lesson_icon: item.pic,
      id: item.id
    };
    this.settings.getValue('topics')
      .then((topics: any) => {
        _.forEach(topics, ltopic => {
          if (!ltopic.lessons)
            ltopic.lessons = [];
          _.forEach(ltopic.lessons, llesson => {
            if (llesson.id == lesson.id && llesson.topicId == ltopic.id)
              topic = ltopic
          })
        });
        lesson.topicId = topic.id;
        this.settings.getValue('subjects')
          .then((subjects: any) => {
            if (!subjects)
              subjects = [];
            _.forEach(subjects, subject => {
              if (!subject.books)
                subject.books = [];
              _.forEach(subject.books, lbook => {
                if (lbook.id == topic.bookId) {
                  book = lbook;
                  book.id = topic.bookId;
                  this.openLesson(lesson, topic, book);
                }
              })
            });
          })
          .catch(() => {
          });
      })
      .catch(() => {
      });
  }

  fetchBookInfo(item) {
    this.settings.getValue('subjects')
      .then((subjects: any) => {
        if (!subjects)
          subjects = [];
        _.forEach(subjects, subject => {
          if (!subject.books)
            subject.books = [];
          _.forEach(subject.books, book => {
            if (book.id == item.id)
              this.openBook(book);
          })
        });
      })
      .catch(() => {
      });
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

}