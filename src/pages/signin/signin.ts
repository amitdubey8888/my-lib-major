import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Home2Page } from '../../pages/home2/home2';
import { SignupPage } from '../../pages/signup/signup';
import { ResetPasswordPage } from '../../pages/reset-password/reset-password';
import { Facebook } from 'ionic-native';
import { GooglePlus } from '@ionic-native/google-plus';
import { Api, User } from '../../providers/providers'
import { Settings } from "../../providers/settings";
import { dataService } from "../../providers/data.service";
import { SubscriptionPage } from '../../pages/subscription/subscription';
import { ProfilePage } from '../../pages/profile/profile';
import { LessonPage } from '../../pages/lesson/lesson';
import { Keyboard } from '@ionic-native/keyboard';


@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})

export class SigninPage implements OnInit {
  public loginForm: any;
  accountInfo = {
    email: '',
    password: '',
    realm: 'user'
  };
  private loader = this.loadingCtrl.create({
    content: "Logging In..."
  });
  public preferences: any = {
    background: ''
  };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public userLogin: User,
    private api: Api,
    public formBuilder: FormBuilder,
    private keyboard: Keyboard,
    public loadingCtrl: LoadingController,
    private toast: ToastController,
    private googlePlus: GooglePlus,
    public dataService: dataService,
    private settings: Settings) {
    this.loginForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });
  }

  ngOnInit() {
    this.fetchAppBackground();
    this.keyboard.disableScroll(true);
  }

  resetPassword() {
    this.navCtrl.push(ResetPasswordPage);
  }

  goBack() {
    this.navCtrl.pop();
  }
  goToNextState() {
    console.log(this.dataService.currentStateName);
    let length = this.navCtrl.length();
    switch (this.dataService.currentStateName) {
      case 'LessonPage': {
        // Check for subscribe here if not subscribe then go to SubscriptionPage and if already subscribe then go to direct. following condition can be checked.
        
          
        if (!this.userLogin.currentUser.subscribed_plan) {
          this.navCtrl.push(SubscriptionPage);
        } else {
          this.navCtrl.push(this.dataService.currentState, this.dataService.currentStateParams);
        }
        this.navCtrl.remove(length-1);
        //this.navCtrl.setPages(this.dataService.currentState, this.dataService.currentStateParams);
        break;
      }
      case 'SubscriptionPage': {
        
        // this.navCtrl.push(this.dataService.currentState);
        this.navCtrl.pop();
        //this.navCtrl.setPages(this.dataService.currentState);
        break;
      }
      case 'ProfilePage': {
        this.navCtrl.push(this.dataService.currentState);
        this.navCtrl.remove(length-1);
        break;
      }
      default: {
        this.navCtrl.setRoot(Home2Page);
      }
    }
  }
  signin() {
    //noinspection TypeScriptUnresolvedFunction
    this.loader.present()
      .then(() => {
      })
      .catch(() => {
        this.showToast(`Please wait...`)
      });
    if (!this.loginForm.valid) {
      //noinspection TypeScriptUnresolvedFunction
      this.loader.dismiss()
        .catch(() => {
        });
      // console.log(this.loginForm.value);
      this.showToast('Invalid Inputs!');
    }
    else {
      this.userLogin.login(this.accountInfo)
        .map(res => res.json())
        .subscribe(
        res => {
          console.log(res);
          if (res.id) {
            this.handleLoggedInUser(res);
          }
          else {
            //noinspection TypeScriptUnresolvedFunction
            this.loader.dismiss()
              .catch(() => {
              });
            this.showToast("Invalid credentials!");
            // console.log("error in login");
          }
        },
        error => {
          this.userLogin.login({
            'username': this.accountInfo.email,
            'password': this.accountInfo.password,
            'realm': 'user'
          })
            .map(res => res.json())
            .subscribe(
            res => {
              console.log("logged in via phone", res);
              if (res.id) {
                this.handleLoggedInUser(res);
              }
              else {
                //noinspection TypeScriptUnresolvedFunction
                this.loader.dismiss()
                  .catch(() => {
                  });
                this.showToast("Invalid credentials!");
                // console.log("error in login");
              }
            },
            error => {
              //noinspection TypeScriptUnresolvedFunction
              this.loader.dismiss()
                .catch(() => {
                });
              this.showToast(error.statusText);
              // console.warn('ERROR in login', error.statusText);
            });
        }
        );
    }
  }

  signup() {
    this.navCtrl.push(SignupPage);
  }

  fbLogin() {
    //noinspection TypeScriptUnresolvedFunction
    this.loader.present()
      .then(() => {
      })
      .catch(() => {
        this.showToast(`Please wait...`)
      });
    //noinspection TypeScriptUnresolvedFunction
    Facebook.login(['email', 'public_profile', 'user_friends'])
      .then(
      (data) => {
        //noinspection TypeScriptUnresolvedFunction
        Facebook.api('/me?fields=email,name&access_token=' + data.authResponse.accessToken, null)
          .then(
          details => {
            this.api.get(`users?filter[where][email]=${details.email}`)
              .map(res => res.json())
              .subscribe(res => {
                if (res.length == 0) {
                  this.userLogin.signup({
                    "password": details.id,
                    "full_name": details.name,
                    "realm": "fbuser",
                    "email": details.email,
                    "emailVerified": true,
                    "profile_picture": "http://graph.facebook.com/" + details.id + "/picture?type=large"
                  })
                    .map(res => res.json())
                    .subscribe(
                    res => {
                      //noinspection TypeScriptUnresolvedFunction
                      this.loader.dismiss()
                        .catch(() => {
                        });
                      if (res.status == 'success') {
                        this.userLogin.loggedIn(res.user);
                        this.showToast(`Logged in as ${details.name}`);
                        // if (this.navCtrl.getViews().length === 1)
                        //   this.navCtrl.setRoot(Home2Page);
                        // else
                        //   this.navCtrl.pop();
                        this.goToNextState();
                      } else {
                        console.error("An error occurred");
                      }
                    },
                    error => {
                      //noinspection TypeScriptUnresolvedFunction
                      this.loader.dismiss()
                        .catch(() => {
                        });
                      this.showToast("Please check your connection!");
                      console.error('Signup ERROR', error);
                    });
                }
                else {
                  this.userLogin.login({
                    "password": details.id,
                    "realm": "fbuser",
                    "email": details.email,
                  })
                    .map(res => res.json())
                    .subscribe(
                    res => {
                      this.handleLoggedInUser(res);
                    },
                    error => {
                      //noinspection TypeScriptUnresolvedFunction
                      this.loader.dismiss()
                        .catch(() => {
                        });
                      console.error('Signup ERROR', error);
                      this.showToast(`User may exist on different login method!`);
                    });
                }
              });
          },
          error => {
            //noinspection TypeScriptUnresolvedFunction
            this.loader.dismiss()
              .catch(() => {
              });
            this.showToast(`${error.errorMessage}`);
          }
          )
      },
      error => {
        //noinspection TypeScriptUnresolvedFunction
        this.loader.dismiss()
          .catch(() => {
          });
        console.log("error in Facebook login", error);
        this.showToast(`${error.errorMessage}`);
      });
  }

  googleLogin() {
    //noinspection TypeScriptUnresolvedFunction
    this.loader.present()
      .then(() => {
      })
      .catch(() => {
        this.showToast(`Please wait...`)
      });
    //noinspection TypeScriptUnresolvedFunction
    this.googlePlus.login({})
      .then(
      details => {
        this.api.get(`users?filter[where][email]=${details.email}`)
          .map(res => res.json())
          .subscribe(res => {
            if (res.length == 0) {
              this.userLogin.signup({
                "password": details.userId,
                "full_name": details.displayName,
                "realm": "googleuser",
                "email": details.email,
                "emailVerified": true,
                "profile_picture": details.imageUrl
              })
                .map(res => res.json())
                .subscribe(
                res => {
                  //noinspection TypeScriptUnresolvedFunction
                  this.loader.dismiss()
                    .catch(() => {
                    });
                  if (res.status == 'success') {
                    this.userLogin.loggedIn(res.user);
                    this.showToast(`Logged in as ${details.displayName}`);
                    // if (this.navCtrl.getViews().length === 1)
                    //   this.navCtrl.setRoot(Home2Page);
                    // else
                    //   this.navCtrl.pop();
                    this.goToNextState();
                  } else {
                    this.showToast("Please check your connection!");
                    // console.error("An error occurred")
                  }
                },
                error => {
                  //noinspection TypeScriptUnresolvedFunction
                  this.loader.dismiss()
                    .catch(() => {
                    });
                  this.showToast("Please check your connection!");
                  console.error('Signup ERROR', error);
                });
            }
            else {
              this.userLogin.login({
                "password": details.userId,
                "realm": "googleuser",
                "email": details.email,
              })
                .map(res => res.json())
                .subscribe(
                res => {
                  this.handleLoggedInUser(res)
                },
                error => {
                  //noinspection TypeScriptUnresolvedFunction
                  this.loader.dismiss()
                    .catch(() => {
                    });
                  console.error('Login ERROR', error);
                  this.showToast(`User may exist on different login method!`);
                });
            }
          });
      })
      .catch(error => {
        //noinspection TypeScriptUnresolvedFunction
        this.loader.dismiss()
          .catch(() => {
          });
        this.showToast(`Unable to login`);
      });
  }

  handleLoggedInUser(token) {
    this.userLogin.fetchUserDetails(token)
      .map(res => res.json())
      .subscribe(
      (profile: any) => {
        //noinspection TypeScriptUnresolvedFunction
        this.loader.dismiss()
          .catch(() => {
          });

        if (profile.banned) {
          this.showToast("User is banned to login!");
          // console.error("banned");
        }
        else {
          if (profile.full_name)
            this.showToast(`Logged in as ${profile.full_name}`);
          else
            this.showToast(`Login Successful`);
          this.userLogin.loggedIn(profile);
          this.settings.setValue('profile', profile);
          // if (this.navCtrl.getViews().length === 1)
          //   this.navCtrl.setRoot(Home2Page);
          // else
          //   this.navCtrl.pop();
          this.goToNextState();
        }
      },
      error => {
        //noinspection TypeScriptUnresolvedFunction
        this.loader.dismiss()
          .catch(() => {
          });
        console.error("unable to fetch user details");
        this.showToast("Oops! Something bad happened, retry");
      })
  }

  showToast(message) {
    let toast = this.toast.create({
      message: message,
      duration: 5000,
      position: 'bottom'
    });
    toast.present();
  }

  fetchAppBackground() {
    this.api.get(`app_preferences?filter[fields][background]=true`)
      .map(res => res.json())
      .subscribe(res => {
        this.preferences = res[0];
      });
  }
}