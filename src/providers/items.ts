import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Item} from '../models/item';
import {Api} from "../providers/api";
import {NetworkCheckService} from "../providers/network.check.service";
import * as _ from 'lodash';
import {Settings} from "../providers/settings";
import {Network} from '@ionic-native/network';

@Injectable()
export class Items {
  items:Item[] = [];

  constructor(public http:Http,
              private settings:Settings,
              private net:Network,
              private network:NetworkCheckService,
              public api:Api) {
    if (this.network.connected) {
      this.getData();
    }
    else {
      this.settings.load()
        .then(() => this.fetchData());
    }

    this.net.onConnect()
      .subscribe(() => this.getData());
    this.net.onDisconnect()
      .subscribe(() => {
        this.settings.load()
          .then(() => this.fetchData());
      })
  }

  query(params?:any) {
    if (!params) {
      return this.items;
    }

    return this.items.filter((item) => {
      for (let key in params) {
        let field = item[key];
        if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
          return item;
        } else if (field == params[key]) {
          return item;
        }
      }
      return null;
    });
  }

  add(item:Item) {
    this.items.push(item);
  }

  delete(item:Item) {
    this.items.splice(this.items.indexOf(item), 1);
  }

  getData() {
    this.items = [];
    this.api.get(`books?filter[fields][book_title]=true&filter[fields][id]=true&filter[fields][book_thumb]=true&filter[fields][book_description]=true`)
      .map(res => res.json())
      .subscribe((response:any) => {
        let item = {};
        _.forEach(response, book => {
          item = {
            name: book.book_title,
            id: book.id,
            category: 'Book',
            pic: book.book_thumb,
            about: book.book_description
          };
          this.items.push(new Item(item));
        });
      });

    this.api.get(`lessons?filter[fields][lesson_title]=true&filter[fields][id]=true&filter[fields][lesson_icon]=true`)
      .map(res => res.json())
      .subscribe((response:any) => {
        let item = {};
        _.forEach(response, lesson => {
          item = {
            name: lesson.lesson_title,
            id: lesson.id,
            category: 'Lesson',
            pic: lesson.lesson_icon
          };
          this.items.push(new Item(item));
        });
      });
  }

  fetchData() {
    this.items = [];
    this.settings.getValue('subjects')
      .then((subjects:any) => {
        if (!subjects)
          subjects = [];
        let item = {};
        _.forEach(subjects, subject => {
          if (!subject.books)
            subject.books = [];
          _.forEach(subject.books, book => {
            item = {
              name: book.book_title,
              id: book.id,
              category: 'Book',
              pic: book.book_thumb,
              about: book.book_description
            };
            this.items.push(new Item(item));
          });
          console.info("Items", this.items)
        });
      })
      .catch(() => {
      });

    this.settings.getValue('lessons')
      .then((lessons:any) => {
        if (!lessons)
          lessons = [];
        let item = {};
        _.forEach(lessons, lesson => {
          item = {
            name: lesson.lesson_title,
            id: lesson.id,
            category: 'Lesson',
            pic: lesson.lesson_icon
          };
          this.items.push(new Item(item));
        })
      })
      .catch(() => {
      });
  }
}