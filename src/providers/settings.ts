import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class Settings {
  private SETTINGS_KEY: string = '_settings';
  public static API_ENDPOINT = 'http://35.184.115.189:8080/api';
  settings: any;
  _defaults: any;
  //noinspection TypeScriptUnresolvedVariable
  _readyPromise: Promise<any>;

  constructor(public storage: Storage, defaults: any) {
    this._defaults = defaults;
  }

  load() {
    //noinspection TypeScriptUnresolvedFunction
    return this.storage.get(this.SETTINGS_KEY).then((value) => {
      console.log(value);
      if (value) {
        this.settings = value;
        this._mergeDefaults(this._defaults);
        return this.settings;
      } else {
        return this.setAll(this._defaults).then((val) => {
          this.settings = val;
        })
      }
    });
  }

  _mergeDefaults(defaults: any) {
    for (let k in defaults) {
      if (!(k in this.settings)) {
        this.settings[k] = defaults[k];
      }
    }
    return this.setAll(this.settings);
  }

  merge(settings: any) {
    for (let k in settings) {
      this.settings[k] = settings[k];
    }
    return this.save();
  }

  setValue(key: string, value: any) {
    this.settings[key] = value;
    return this.storage.set(this.SETTINGS_KEY, this.settings);
  }

  setAll(value: any) {
    return this.storage.set(this.SETTINGS_KEY, value);
  }

  getValue(key: string) {
    //noinspection TypeScriptUnresolvedFunction
    return this.storage.get(this.SETTINGS_KEY)
      .then(settings => {
        return settings[key];
      });
  }

  save() {
    return this.setAll(this.settings);
  }

  get allSettings() {
    return this.settings;
  }

  clear() {
    this.storage.clear();
  }
}