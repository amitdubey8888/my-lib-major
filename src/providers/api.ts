//noinspection TypeScriptCheckImport
import {Injectable} from '@angular/core';
//noinspection TypeScriptCheckImport
import {Http, RequestOptions, URLSearchParams, Headers} from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * Api is a generic REST Api handler. Set your API url first.
 */
@Injectable()
export class Api {
  url:string = "http://35.184.115.189:8080/api";  //prod
  // url: string = "http://104.198.106.68:8080/api";   //dev
  // url: string = "http://0.0.0.0:8080/api";  //local

  payurl:string = "http://35.184.115.189:8080";

  constructor(public http:Http) {
  }

  get(endpoint:string, params?:any, options?:RequestOptions) {
    if (!options) {
      options = new RequestOptions();
    }

    // Support easy query params for GET requests
    if (params) {
      let p = new URLSearchParams();
      for (let k in params) {
        p.set(k, params[k]);
      }
      // Set the search field if we have params and don't already have
      // a search field set in options.
      //noinspection TypeScriptUnresolvedVariable
      options.search = !options.search && p || options.search;
    }
    return this.http.get(this.url + '/' + endpoint, options);
  }

  post(endpoint:string, body:any, options?:RequestOptions) {
    return this.http.post(this.url + '/' + endpoint, body, options);
  }

  put(endpoint:string, body:any, options?:RequestOptions) {
    return this.http.put(this.url + '/' + endpoint, body, options);
  }

  delete(endpoint:string, body:any, options?:RequestOptions) {
    return this.http.post(this.url + '/' + endpoint, body, options);
  }

  patch(endpoint:string, body:any, options?:RequestOptions) {
    return this.http.patch(this.url + '/' + endpoint, body, options);
  }

  pay(endpoint:string, body:any, options?:RequestOptions) {
    let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    let noptions = new RequestOptions({headers: headers});

    let nbody = Api.convertJsonToFormData(body);
    return this.http.post(this.payurl + '/' + endpoint, nbody, noptions);
  }

  static convertJsonToFormData(jsonData) {
    var str = [];
    for (var p in jsonData)
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(jsonData[p]));
    return str.join("&");
  }
}