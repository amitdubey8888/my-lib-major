import {Injectable} from "@angular/core";
import {Network} from '@ionic-native/network';


@Injectable()
export class NetworkCheckService {

  public connected:boolean = false;

  constructor(private net:Network) {
    this.net.onDisconnect().subscribe(() => {
      this.connected = false;
    });

    this.net.onConnect().subscribe(() => {
      this.connected = true;
    });

    console.info(this.net.type);

    if (this.net.type || this.net.type != 'none') {
      this.connected = true;
    }
  }
}