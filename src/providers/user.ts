import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Api} from './api';
import {GooglePlus} from '@ionic-native/google-plus';
import {Facebook} from 'ionic-native';
import {Settings} from "./settings";

@Injectable()
export class User {

  public currentUser:any;

  constructor(public http:Http,
              public api:Api,
              public settings: Settings,
              public googlePlus:GooglePlus) {
  }

  login(accountInfo:any) {
    let seq = this.api.post('users/login', accountInfo).share();
    seq
      .map(res => res.json())
      .subscribe(res => {
        // console.log("Success")
      }, err => {
        console.error('ERROR', err)
      });
    return seq;
  }

  signup(accountInfo:any) {
    let seq = this.api.post('users', accountInfo).share();
    seq
      .map(res => res.json())
      .subscribe(res => {
        if (res.status == 'success') {
          this.loggedIn(res.user);
        } else {
          console.error("An error occurred")
        }
      }, err => {
        console.error('ERROR', err)
      });
    return seq;
  }

  resetPassword(email:string) {
    return this.api.post('users/reset', {"email": email});
  }

  logout() {
    console.log(this.currentUser);
    if(this.currentUser.realm === 'googleuser') {
      this.googlePlus.logout();
    } else if(this.currentUser.realm === 'fbuser') {
      Facebook.logout();
    }
    // let seq = this.api.post('users/logout', null).share();
    // seq
    //   .map(res => res.json())
    //   .subscribe(res => {
    //      console.log("Success", res)
    //   }, err => {
    //     console.error('ERROR', err)
    //   });
    this.currentUser = null;
    this.settings.setValue('profile', null);
    this.settings.clear();
  }

  loggedIn(resp) {
    this.currentUser = resp;
    console.info("currentUser", resp);
  }

  fetchUserDetails(token) {
    return this.api.get(`users/${token.userId}`, {'access_token': token.id})
  }
}