import {Facebook} from 'ionic-native';

export class FbLoginService {
  fbLogin() {
    //noinspection TypeScriptUnresolvedFunction
    Facebook.login(['email', 'public_profile']).then(this.successCallback, this.errorCallBack);
  }

  successCallback(data) {
    //noinspection TypeScriptUnresolvedFunction
    Facebook.api('/me?fields=email,name&access_token=' + data.authResponse.accessToken, null)
      .then(profileInfo=> {
        console.log(profileInfo);
        //noinspection TypeScriptUnresolvedVariable,TypeScriptUnresolvedFunction
        return Promise.resolve(profileInfo);
      }, err=> console.log(err));
  }

  errorCallBack(error) {
    console.log("error in Facebook login");
    //noinspection TypeScriptUnresolvedVariable,TypeScriptUnresolvedFunction
    return Promise.reject(error);
  }
}