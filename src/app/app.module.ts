import { NgModule, ErrorHandler } from '@angular/core';
import { Http } from '@angular/http';
import { FormsModule } from '@angular/forms';
import {AngularFireModule} from 'angularfire2';
import * as firebase from 'firebase';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Toast } from '@ionic-native/toast';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Network } from '@ionic-native/network';
import { Transfer } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { Keyboard } from '@ionic-native/keyboard';
import { VideoEditor } from '@ionic-native/video-editor';

// files for video players
import { VgCoreModule } from 'videogular2/core';
import { VgControlsModule } from 'videogular2/controls';
import { VgOverlayPlayModule } from 'videogular2/overlay-play';
import { VgBufferingModule } from 'videogular2/buffering';

// end


import { MyApp } from './app.component';

import { Home2Page } from '../pages/home2/home2';
import { BookPage } from '../pages/book/book';
import { LessonPage } from '../pages/lesson/lesson';
import { SigninPage } from '../pages/signin/signin';
import { ShareModalPage } from '../pages/share-modal/share-modal';
import { SignupPage } from '../pages/signup/signup';
import { ProfilePage } from '../pages/profile/profile';
import { ChooseNetworkPage } from '../pages/choose-network/choose-network';
import { ChoosePaymentPage } from '../pages/choose-payment/choose-payment';
import { SubscriptionPage } from '../pages/subscription/subscription';

import { LicensePage } from '../pages/license/license';
import { HelpPage } from '../pages/help/help';
import { PrivacyPage } from '../pages/privacy/privacy';
import { EnterPhonePage } from '../pages/enter-phone/enter-phone';
import { TermsPage } from '../pages/terms/terms';
import { MenuPage } from '../pages/menu/menu';
import { SettingsPage } from '../pages/settings/settings';
import { WindowRef } from '../providers/window-ref';
import { User } from '../providers/user';
import { Api } from '../providers/api';
import { FbLoginService } from '../providers/fbLoginService';
import { NetworkCheckService } from "../providers/network.check.service";
import { Settings } from '../providers/settings';
import { Items } from '../providers/items';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';

import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { GooglePlus } from '@ionic-native/google-plus';
import { Stripe } from '@ionic-native/stripe';
import { CardCheckoutPage } from "../pages/card-checkout/card-checkout";
import { Firebase } from '@ionic-native/firebase';

import { LazyImgComponent } from '../pages/lazy-img/lazy-img.component';
import { dataService } from '../providers/data.service';
import { BackgroundMode } from '@ionic-native/background-mode';

// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    // option1: true,
    // option2: 'Ionitron J. Framework',
    // option3: '3',
    // option4: 'Hello'
  });
}


/**
 * The Pages array lists all of the pages we want to use in our app.
 * We then take these pages and inject them into our NgModule so Angular
 * can find them. As you add and remove pages, make sure to keep this list up to date.
 */
let pages = [
  MyApp,
  SigninPage,
  LicensePage,
  SignupPage,
  HelpPage,
  PrivacyPage,
  TermsPage,
  MenuPage,
  SettingsPage,
  ProfilePage,
  Home2Page,
  BookPage,
  LessonPage,
  ShareModalPage,
  SubscriptionPage,
  ChoosePaymentPage,
  ChooseNetworkPage,
  EnterPhonePage,
  ResetPasswordPage,
  CardCheckoutPage,
  LazyImgComponent,
];

// Must export the config
export const firebaseConfig = {
  apiKey: "AIzaSyD7aUdLI1QjB_js7t8fBnSygHc2MecyIIQ",
  authDomain: "my-lib-project-46754.firebaseapp.com",
  databaseURL: "https://my-lib-project-46754.firebaseio.com",
  projectId: "my-lib-project-46754",
  storageBucket: "my-lib-project-46754.appspot.com",
  messagingSenderId: "515480162438"
};
firebase.initializeApp(firebaseConfig);


export function declarations() {
  return pages;
}

export function entryComponents() {
  return pages;
}

export function providers() {
  return [
    Storage,
    User,
    FbLoginService,
    NetworkCheckService,
    Api,
    Items,
    WindowRef,
    Toast,
    GooglePlus,
    Firebase,
    Stripe,
    SocialSharing,
    dataService,
    Network,
    File,
    Transfer,
    BackgroundMode,
    VideoEditor,
    Keyboard,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations(),
  imports: [
    FormsModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp, { scrollAssist: false, autoFocusAssist: false }),
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule {
}
