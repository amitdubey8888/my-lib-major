import {Component, ViewChild} from '@angular/core';
import {Platform, Nav, Config} from 'ionic-angular';
import {StatusBar, Splashscreen} from 'ionic-native';
import {SigninPage} from '../pages/signin/signin';
import {SubscriptionPage} from '../pages/subscription/subscription';
import {Firebase} from '@ionic-native/firebase';
import {Settings, User} from '../providers/providers';
import {Home2Page} from '../pages/home2/home2';
import {FirstRunPage} from '../pages/pages';
import {SettingsPage} from '../pages/settings/settings';
import {HelpPage} from '../pages/help/help';
import {TranslateService} from 'ng2-translate/ng2-translate';
import {dataService} from "../providers/data.service";
//noinspection TypeScriptCheckImport
import ImgCache from 'imgcache.js';
import {BackgroundMode} from '@ionic-native/background-mode';

@Component({
  template: `<ion-menu [content]="content">
    <ion-content class="sidemenu">
      <ion-list no-lines>
      <ion-item>
        <button menuClose ion-button icon-left clear color="light" (click)="openPage(pages[0])">
            <ion-icon name="home"></ion-icon>
          Home
        </button>
        </ion-item>
      <ion-item>

        <button menuClose ion-button icon-left clear color="light" (click)="openPage(pages[2])"> 
            <ion-icon name="ios-albums-outline"></ion-icon>
          Subscriptions
        </button>
        </ion-item>
      <ion-item>

        <button menuClose ion-button icon-left clear color="light" (click)="openPage(pages[1])">
            <ion-icon name="ios-cog-outline"></ion-icon>
          Settings
        </button>
        </ion-item>
      <ion-item>

        <button menuClose ion-button icon-left clear color="light" (click)="openPage(pages[3])">
            <ion-icon name="ios-help-circle-outline"></ion-icon>
          Help
        </button>
        </ion-item>
      <ion-item>
        <button *ngIf="this.user.currentUser" menuClose ion-button icon-left clear color="light" (click)="logout()"> 
            <ion-icon name="md-log-out"></ion-icon>
          Signout
        </button>
        <button *ngIf="!this.user.currentUser" menuClose ion-button icon-left clear color="light" (click)="login()"> 
            <ion-icon name="md-log-in"></ion-icon>
          Signin
        </button>
      </ion-item>
      </ion-list>
    </ion-content>

  </ion-menu>
  <ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  public rootPage = FirstRunPage;

  @ViewChild(Nav) nav:Nav;

  pages:any[] = [
    {title: 'Home', component: Home2Page},
    {title: 'Settings', component: SettingsPage},
    {title: 'Subscription', component: SubscriptionPage},
    {title: 'Help', component: HelpPage}
  ];

  constructor(translate:TranslateService,
              platform:Platform,
              config:Config,
              private firebase:Firebase,
              private backgroundMode:BackgroundMode,
              private settings:Settings,
              public dataService:dataService,
              public user:User) {
    // Set the default language for translation strings, and the current language.
    translate.setDefaultLang('en');
    translate.use('en');

    translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });

    //noinspection TypeScriptUnresolvedFunction
    platform.ready().then(() => {
      // ImgCache.options.debug = true;
      //noinspection TypeScriptUnresolvedVariable
      ImgCache.options.skipURIencoding = true;
      // page is set until img cache has started
      //noinspection TypeScriptUnresolvedFunction
      ImgCache.init(() => {
          console.info('ImgCache init: success!')
        },
        () => {
          console.info('ImgCache init: error! Check the log for errors');
        });

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();

      this.backgroundMode.setDefaults({
        title: 'MyLib',
        text: 'Download in progress',
        icon: 'icon', // this will look for icon.png in platforms/android/res/drawable|mipmap
        color: 'F14F4D' // hex format like 'F14F4D'
      });
      this.backgroundMode.enable();


      Splashscreen.hide();
      this.checkLogin();
      //noinspection TypeScriptUnresolvedFunction
      this.firebase.getToken()
        .then(token => console.log(`The token is ${token}`)) // save the token server-side and use it to push notifications to this device
        .catch(error => console.error('Error getting token', error));

      //noinspection TypeScriptUnresolvedFunction
      this.firebase.onNotificationOpen()
        .map(res => res.json())
        .subscribe(
          data => console.log("fire", data),
          error => console.error('Error on not open', error)
        );

      this.firebase.logEvent('notification opened', {'test': 'checking analytics'});

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  logout() {
    this.user.logout();
    this.nav.setRoot(Home2Page);
  }

  login() {
    this.dataService.currentStateName = 'Home2Page';
    this.dataService.currentState = Home2Page;
    console.log(this.dataService.currentState);
    this.nav.push(SigninPage);
  }

  checkLogin() {
    this.settings.load()
      .then((data) => {
        console.log(data);
        if (data != {} && data != null) {
          this.settings.getValue('profile')
            .then((profile) => {
              this.user.currentUser = profile
            })
        }
      });
  }
}
